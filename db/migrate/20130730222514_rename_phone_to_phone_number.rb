class RenamePhoneToPhoneNumber < ActiveRecord::Migration
  def change
    rename_table :phones, :phone_numbers
  end
end
