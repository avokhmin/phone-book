class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :number
      t.integer :user_id
      t.string :name

      t.timestamps
    end
    add_index :phones, [:user_id, :number], :unique => true
  end
end
