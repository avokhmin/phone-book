FactoryGirl.define do
  factory :user do
    email { FactoryGirl.generate(:email) }
    password '12345678'
    password_confirmation {|u| u.password}
  end
end