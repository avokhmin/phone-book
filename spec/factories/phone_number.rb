FactoryGirl.define do
  factory :phone_number do
    association :user, :factory => :user
    name    { FactoryGirl.generate(:string) }
    number  { FactoryGirl.generate(:integer).to_s }
  end
end