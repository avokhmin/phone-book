require 'spec_helper'

describe PhoneNumber do

  let!(:phone_number) { FactoryGirl.create(:phone_number) }

  it { should belong_to(:user) }

  it { should validate_presence_of(:user_id) }
  it { should validate_presence_of(:number) }
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:user_id).scoped_to(:number) }

  it { should ensure_length_of(:name).is_at_most(255) }
  it { should ensure_length_of(:number).is_at_most(30) }

end
