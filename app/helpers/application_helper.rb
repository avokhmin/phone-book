module ApplicationHelper

  # See: https://github.com/mislav/will_paginate/wiki/Link-renderer
  # change the default link renderer for will_paginate
  def will_paginate(collection_or_options = nil, options = {})
    if collection_or_options.is_a? Hash
      options, collection_or_options = collection_or_options, nil
    end
    options.merge!(renderer: FoundationPaginationHelper::LinkRenderer) unless options[:renderer]
    options.merge!(next_label: '»') unless options[:next_label]
    options.merge!(previous_label: '«') unless options[:previous_label]
    super *[collection_or_options, options].compact
  end

end
