require 'csv'
class PhoneNumber < ActiveRecord::Base
  belongs_to :user

  validates :number,  :user_id, :name, presence: true
  validates :number, uniqueness: {scope: :user_id}

  validates :number,  format: {with: /\A[\+\*]?(\([\d]+\))?[\-\.\s\d]+\Z/i},
                      length: {maximum: 30}

  validates :name, length: {maximum: 255}

  default_scope { order(:number) }

  def self.export(user)
    # tab-separated
    csv_string = CSV.generate(force_quotes: true, col_sep: "\t") do |csv|
      # header row
      #csv << ["number", "name"]

      # data rows
      user.phone_numbers.pluck(:number, :name).each do |phone_number|
        csv << phone_number
      end
    end
    csv_string
  end

  def self.import(file, user)
    CSV.foreach(file.path, force_quotes: true, col_sep: "\t") do |row|
      puts row.inspect
      phone_number = user.phone_numbers.find_or_initialize_by_number(row[0])
      phone_number.name = row[1]
      phone_number.save!
    end
    return true
  rescue => e
    return false
  end

end
