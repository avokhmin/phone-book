require 'csv'
class PhoneNumbersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_phone_number, only: [:show, :edit, :update, :destroy]

  # GET /phone_numbers
  def index
    phone_numbers
    render partial: 'table' if request.xhr?
  end

  # GET /phone_numbers/1
  def show
  end

  # GET /phone_numbers/new
  def new
    @phone_number = PhoneNumber.new
  end

  # GET /phone_numbers/1/edit
  def edit
  end

  def download
    send_data PhoneNumber.export(current_user),
      :type => 'text/csv; charset=iso-8859-1; header=present',
      :disposition => "attachment; filename=phone-numbers#{Time.now.utc.strftime("%Y-%m-%d")}.csv"
  end

  def upload
  end

  def import
    if PhoneNumber.import(params[:file], current_user)
      flash.now[:notice] = 'Phone numbers was successfully imported.'
    else
      flash.now[:error] = 'Wrong format!'
    end
  end

  # POST /phone_numbers
  def create
    @phone_number = PhoneNumber.new(phone_number_params)
    @phone_number.user = current_user

    if @phone_number.save
      redirect_to @phone_number, notice: 'Phone number was successfully created.'
    else
      flash.now[:error] = @phone_number.errors.full_messages
      render action: 'new'
    end
  end

  # PATCH/PUT /phone_numbers/1
  def update
    if @phone_number.update(phone_number_params)
      redirect_to @phone_number, notice: 'Phone number was successfully updated.'
    else
      flash.now[:error] = @phone_number.errors.full_messages
      render action: 'edit'
    end
  end

  # DELETE /phone_numbers/1
  def destroy
    @phone_number.destroy
    flash[:notice] = "Phone number was successfully destroyed."
    if request.xhr?
      phone_numbers
      render partial: 'table'
    else
      redirect_to phone_numbers_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phone_number
      @phone_number = PhoneNumber.find(params[:id])
      # TODO: use cancan gem
      not_found if @phone_number.user != current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phone_number_params
      params.require(:phone_number).permit(:number, :name)
    end

    def phone_numbers
      @phone_numbers = current_user.phone_numbers.paginate(paginate_params)
    end
end
