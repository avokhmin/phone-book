class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :has_layout?

  protected

  def has_layout?
    request.xhr? ? 'ajax' : 'application'
  end

  def paginate_params
    per_page = params[:per_page].to_i
    per_page = 3 if per_page < 1
    per_page = 100 if per_page >100
    page = params[:page].to_i
    page = nil if page == 0
    {:page => page, :per_page => per_page}
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

end
